// Assembler Projekt 3 Fractal Paprotka Mateusz Kulpa
//	prawdopodobie�stwo      Wsp�czynniki odwzorowania
//	1%	a=0		b=0			c=0		d=0		e=0.16	f=0
//	79% a=0.85	b=0.04		c=0		d=-0.04 e=0.85	f=1.6
//	10% a=0.2	b=-0.26		c=0		d=0.23	e=0.22	f=1.6
//	10%	a=-0.15 b=0.28		c=0		d=0.26	e=0.24	f=0.44
// zakres wy�wietlanych punkt�w na ekranie: xmin=0 xmax=10,5 ymin=-3,0 ymax=2,8
#include <windows.h>
#include <stdio.h>
#include <iostream>
#include <cmath>
#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>

typedef void(__stdcall *MYPROC_dllOK)(void);
typedef void(__stdcall *MYPROC_AffineTransformation)(double*, int*, int);
typedef void(__stdcall *MYPROC_Conversion)(double*, int, int, int);

// CONSTANTS
 double minX = 0.0;
 double maxX = 10.5;
 double minY = -3.0;
 double maxY = 2.8;


 int a = 13100233;
 int b = 11040857;
 int m = 9999991;

int L1 = 1;
int L2 = 10000000;

int screenWidth = 640;
int screenHeight = 480;

int OperationCount = 1000000;
double *calculatesValues = new double[OperationCount * 2];

int main()
{
	ALLEGRO_DISPLAY *display = NULL;
	HINSTANCE hDLL;
	MYPROC_AffineTransformation AffineTransformation;
	MYPROC_Conversion Conversion;
	hDLL = LoadLibrary("MojaDLL.dll");
	if (hDLL == NULL) exit(0);
	AffineTransformation = (MYPROC_AffineTransformation)GetProcAddress(hDLL, "AffineTransformation");
	Conversion = (MYPROC_Conversion)GetProcAddress(hDLL, "Conversion");
	if (!al_init()) {
		fprintf(stderr, "failed to initialize allegro!\n");
		return -1;
	}

	display = al_create_display(screenWidth, screenHeight);

	if (!display) {
		fprintf(stderr, "failed to create display!\n");
		return -1;
	}
	al_init_primitives_addon();
	
	
	int *random_numbers = new int[OperationCount];
	srand(time(NULL));
	for (int i = 0; i < OperationCount; i++) {
		random_numbers[i] = (int)abs((((a*rand() + b) % m) % L2) + L1);
	}

	calculatesValues[0] = 1.0;
	calculatesValues[1] = 1.0;	

	/*
	for (int x = 2, y=0; x < OperationCount; x = x + 2, y++) {
			if (random_numbers[y] < 100000) {
				//1%	a=0		b=0			c=0		d=0		e=0.16	f=0
				calculatesValues[x] = 0 * calculatesValues[x - 2] + 0 * calculatesValues[x - 1] + 0;
				calculatesValues[x + 1] = 0 * calculatesValues[x - 2] + 0.16*calculatesValues[x - 1] + 0;
			}
			else if (random_numbers[y] < 8000000) { 
				//	79% a=0.85	b=0.04		c=0		d=-0.04 e=0.85	f=1.6
				calculatesValues[x] = 0.85 * calculatesValues[x - 2] + 0.04 * calculatesValues[x - 1] + 0;
				calculatesValues[x + 1] = -0.04 * calculatesValues[x - 2] + 0.85*calculatesValues[x - 1] + 1.6;
				}
			else if (random_numbers[y] < 9000000) {
				//	10% a=0.2	b=-0.26		c=0		d=0.23	e=0.22	f=1.6
				calculatesValues[x] = 0.2 * calculatesValues[x - 2] + -0.26 * calculatesValues[x - 1] + 0;
				calculatesValues[x + 1] = 0.23 * calculatesValues[x - 2] + 0.22*calculatesValues[x - 1] + 1.6;
			}
			else {
				//10%	a=-0.15 b=0.28		c=0		d=0.26	e=0.24	f=0.44
				calculatesValues[x] = -0.15 * calculatesValues[x - 2] + 0.28 * calculatesValues[x - 1] + 0;
				calculatesValues[x + 1] = 0.26 * calculatesValues[x - 2] + 0.24*calculatesValues[x - 1] + 0.44;
			}
	} */
	//proc AffineTransformation stdcall, wsk_values:DWORD, wsk_randoms:DWORD, operationCount:DWORD
	AffineTransformation(calculatesValues, random_numbers, OperationCount);
	
	// proc Conversion stdcall, wsk_values:DWORD, minX:DWORD, minY:DWORD, maxX:DWORD, maxY:DWORD, screenWidth:DWORD, screenHeight:DWORD, OperationCount:DWOR
	Conversion(calculatesValues, screenWidth, screenHeight, OperationCount);
	/*for (int x = 0; x < OperationCount * 2; x = x + 2) {
		double temp1 = 0-(((calculatesValues[x] - minX) / (maxX - minX))*screenWidth) / 2;
		double temp2 = ((((calculatesValues[x + 1] - minY) / (maxY - minY))*screenHeight) - screenHeight) / 2;
		calculatesValues[x] = temp2;
		calculatesValues[x + 1] = temp1;
	}*/
	
	al_draw_filled_rectangle(0, 0, 1000, 1000, al_map_rgb(255, 255, 255));
	ALLEGRO_COLOR color = al_map_rgb(0, 0, 0);
	for (int x = 0; x < OperationCount * 2; x = x + 2) {
		al_draw_pixel((int)calculatesValues[x]+screenWidth/2-100, (int)calculatesValues[x + 1] + screenHeight/2, color);

	}

	al_flip_display();
	al_rest(20.0);
	al_destroy_display(display);
	return 0;
}